import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		final BeanFactory Bf = new ClassPathXmlApplicationContext("application.xml");
	    final User utente = (User) Bf.getBean("Utente");
	    
	    System.out.println(utente.getFullName());
	    System.out.println(utente.getAddress().toString());
	}

}
